//
//  CategoriesModel.swift
//  news
//
//  Created by Rastislav Smolen on 28/10/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import Foundation

   struct Categories: Codable
   {
       let status: String
       let sources: [Sources]
    
   }


   struct Sources: Codable {
       let id, name, sourceDescription: String
       let url: String
       let category: Category
       let language, country: String

       enum CodingKeys: String, CodingKey {
           case id, name
           case sourceDescription = "description"
           case url, category, language, country
       }
   }

   enum Category: String, Codable {
       case business = "business"
       case entertainment = "entertainment"
       case general = "general"
       case health = "health"
       case science = "science"
       case sports = "sports"
       case technology = "technology"
   }
   
   struct CategoriesForTableView
   {
    let business : String
    let entertainment : String
    let general :String
    let health : String
    let science : String
    let sports : String
    let technology : String
    
}
struct Sections
{
    //var nameOfTheCategories = [String]()

}
class CategoriesModel
{
    var categories : [CategoriesForTableView]!
   // var sections : [Sections]!
     var nameOfTheCategories = [String]()
    
    init()
    {
       categories = []
    
        nameOfTheCategories = ["General","Sports","Bussiness","Technology","Entertainmnent","Science"]
        
    }
    
    
     func IntializeData(completion: @escaping () -> Void)
         {
    //        var name : String?
    //        var headline:String?
    //        var image : String?
            
         let jsonUrlString = "https://newsapi.org/v2/sources?category&apiKey=56dd0571655e431086268acbfb945fcc"
           // let imageString =  URLComponents()
             guard let url = URL(string: jsonUrlString) else {return}
         
             URLSession.shared.dataTask(with: url)
             {(data,respondse,err) in
             //   guard let name = name , headline , image else {return}
                 guard let data = data else {return}
            //    let imageURl = URL(string:)
            //    guard let name = name else {return}
              //  guard let headline = headline else {return}
              //  self.sections.append(Sections(nameOfTheCategories: ["General","Sports","Bussiness","Technology","Entertainmnent","Science"]))
                 do
                 {
                     let  top = try JSONDecoder().decode(Categories.self, from: data)
                    
                    for element in top.sources
                    {
                        //var imageUrl = NSURL(string: element.urlToImage!)
                        
                        //print(website)
                        print(element.category)
                    }
                    
                    completion()
                     
                 }catch
                 {
                      print(data)
                         print(error)
                 }
             }.resume()
            
      
         
        }

}
