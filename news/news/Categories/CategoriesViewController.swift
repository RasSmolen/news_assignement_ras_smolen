//
//  CategoriesViewController.swift
//  news
//
//  Created by Rastislav Smolen on 28/10/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController
    
{
    var model : CategoriesModel!
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        model = CategoriesModel()
        model.IntializeData {
            DispatchQueue.main.async
                {
                   self.tableView.reloadData()
                    
            }
        }
    }
    

}
extension CategoriesViewController : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.nameOfTheCategories.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let controller = ViewController()
        navigationController?.popToViewController(controller, animated: true)

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath)
        
        
        cell.textLabel?.text = model.nameOfTheCategories[indexPath.row]
        
        return cell
    }
}
