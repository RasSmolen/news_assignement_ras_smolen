//
//  ViewController.swift
//  news
//
//  Created by Rastislav Smolen on 27/10/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import UIKit


class CellController : UITableViewCell
{
    @IBOutlet var authorLabel: UILabel!
    
    @IBOutlet var dateLabe: UILabel!
    
    @IBOutlet var headlineLabel: UILabel!
    
    @IBOutlet var texfield: UITextView!
    
    
    @IBOutlet var articlePicture: UIImageView!
}
class ViewController: UIViewController
{
    var model : News_Model!
    var searchModel : NewSearchViewController!
    
    @IBOutlet var tableView: UITableView!
    
    
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        model = News_Model()
        searchModel = NewSearchViewController()
        model.IntializeData {
            DispatchQueue.main.async
                {
                    self.tableView.reloadData()
                    
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        // Attempt to both safely get the search term and URL encode it
        
    }
}
extension ViewController : UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.news.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        guard let selectedItem = model.news[indexPath.row].website else
        {
            return print("website not found")
        }
       
        let url = selectedItem
        
        UIApplication.shared.open(url)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! CellController
        
        cell.authorLabel.text = model.news[indexPath.row].name
        cell.headlineLabel.text = model.news[indexPath.row].headline
        cell.dateLabe.text = model.news[indexPath.row].date
        cell.texfield.text = model.news[indexPath.row].description
        
        
        return cell
    }
}

