//
//  News_Model.swift
//  news
//
//  Created by Rastislav Smolen on 27/10/2019.
//  Copyright © 2019 Rastislav Smolen. All rights reserved.
//

import Foundation
import UIKit

struct Root: Codable {
  
    let articles: [Article]
}

struct Article: Codable {
    let source: Source
    let author: String?
    let title, articleDescription: String?
    let url: URL?
    let urlToImage: String?
    let publishedAt: StringLiteralType

    enum CodingKeys: String, CodingKey {
        case source, author, title
        case articleDescription = "description"
        case url, urlToImage , publishedAt
    }
}

struct Source: Codable
{
   
    let name: String
}

struct News
{
   
    var name : String?
    var headline : String?
    var image : String?
    var date  : StringLiteralType
    var website : URL?
    var description : String?
    
}

class News_Model
{
    var news = [News]()
    init()
        
    {
        news = []
    }
   
    
    func IntializeData(completion: @escaping () -> Void)
     {
//        var name : String?
//        var headline:String?
//        var image : String?
        
     let jsonUrlString = "https://newsapi.org/v2/top-headlines?country=gb&apiKey=56dd0571655e431086268acbfb945fcc"
       // let imageString =  URLComponents()
         guard let url = URL(string: jsonUrlString) else {return}
     
         URLSession.shared.dataTask(with: url)
         {(data,respondse,err) in
         //   guard let name = name , headline , image else {return}
             guard let data = data else {return}
        //    let imageURl = URL(string:)
        //    guard let name = name else {return}
          //  guard let headline = headline else {return}
          
             do
             {
                 let  top = try JSONDecoder().decode(Root.self, from: data)
                
                for element in top.articles
                {
                    //var imageUrl = NSURL(string: element.urlToImage!)
                    self.news.append(News(name: element.author, headline: element.title, image: element.urlToImage,date: element.publishedAt,website: element.url ,description: element.articleDescription))
                    guard let website = element.url else {return}
                    //print(website)
                                
                }
                
                completion()
                 
             }catch
             {
                  print(data)
                     print(error)
             }
         }.resume()
        
  
     
    }

    
}
